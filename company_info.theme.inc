<?php


/**
 * Theme individual info's item.
 */
function theme_company_info_item($variables) {

  $type = $variables['type'];
  $item = $variables['item'];
  $key = $variables['key'];
  
  if (!$item) { return ''; }
  
  $output = '<div class="company-info-item item-' . $key . ' ' . $type . '">';
  switch ($type) {
    case 'phones' :
      $output .= theme('company_info_item_phones', array('item' => $item, 'key' => $key));
    break;
    case 'social' :
      $output .= theme('company_info_item_social', array('item' => $item, 'key' => $key));
    break;
    case 'messenger' :
      $output .= theme('company_info_item_messenger', array('item' => $item, 'key' => $key));
    break;
    case 'maps' :
      $output .= theme('company_info_item_maps', array('item' => $item, 'key' => $key));
    break;
    case 'misc' :
      $output .= theme('company_info_item_misc', array('item' => $item, 'key' => $key));
    break;
  }
 
  if (user_access('administer company info') && $variables['admin_link']) {
    $output .= l(t('Remove'), "company-info/remove/$type/$key", array(
      'query' => array('token' => drupal_get_token(), 'destination' => $_GET['q']),
      'attributes' => array(
        'class' => array('remove'))));
  }
  
  $output .= '</div>';
 return $output;
}

/**
 * Theme an individual info's column.
 */
function theme_company_info_column($variables) {
  $output = '<div class="column ' . $variables['type'] . ' pos-' . $variables['position'] . '">';
  $output .= '<div class="column-label">' . check_plain($variables['items']['label']) . '</div>';
  foreach ($variables['items']['items'] as $key => $value) {
    $output .= theme('company_info_item', array('key' => $key, 'item' => $value, 'type' => $variables['type']));
  }
  $output .= '</div>';
  return $output;
}

/**
 * Theme copyright.
 */
function theme_company_info_item_copyright($variables) {

  $settings = $variables['settings'];
  $name = !empty($settings['company']['name']) ? check_plain($settings['company']['name']) : '';
  $copyright = !empty($settings['company']['copyright']) ? $settings['company']['copyright'] : '%symbol %years %company_name All Rights Reserved';
  
  $year_current = date('Y');
  $years = '<span class="current">' . check_plain($year_current) . '</span>';
  
  if (!empty($settings['company']['born'])) {
    $year_start = intval($settings['company']['born']);
    if($year_start == $year_current){
      $years = '<span class="start">' . check_plain($year_start) . '</span>';
    }
    if($year_start && $year_start < $year_current){
      $years = '<span class="start">' . check_plain($year_start) . '</span> - <span class="current">' . check_plain($year_current) . '</span>';
    }
  }
  
  $string = str_replace('%years', $years, $copyright);
  $string = str_replace('%symbol', "&copy;", $string);
  $string = str_replace('%company_name', $name, $string);
  return (trim($string)) ? '<span class="company-info copyright">' . $string . '</span>' : '';
}

/**
 * Theme company name item.
 */
function theme_company_info_item_name($variables) {
  return $variables['string'] ? '<span class="company-info name">' . check_plain($variables['string']) . '</span>' : '';
}

/**
 * Theme company phone item.
 */
function theme_company_info_item_phones($variables) { 
  $item = $variables['item'];
  $key = $variables['key'];

  $output = '<div class="item-' . $key . '">';
  $output .= '<span class="phone label">' . check_plain($item['label']) . '</span>';
  $output .= ': ';
  $output .= '<span class="phone text">' . check_plain($item['number']) . '</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme company text item.
 */
function theme_company_info_item_misc($variables) { 
  $item = $variables['item'];
  $key = $variables['key'];
  
  $output = '<div class="item-' . $key . '">';
  $output .= '<div class="misc text">' . filter_xss($item['misc']) . '</div>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme company social link item.
 */
function theme_company_info_item_social($variables) { 
  $item = $variables['item'];
  $key = $variables['key'];
  
  $output = '<div class="item-' . $key . '">';
  $output .= l(theme('image', array(
    'path' => file_create_url($item['icon']))),
    check_url($item['link']), array('html' => TRUE, 'attributes' => array('rel' => 'nofollow')));
  $output .= '<span class="social link">' . l($item['label'], check_url($item['link']), array(
    'attributes' => array('rel' => 'nofollow'))) . '</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme company messenger item.
 */
function theme_company_info_item_messenger($variables) { 
  $item = $variables['item'];
  $key = $variables['key'];
  
  $output = '<div class="item-' . $key . '">';
  $output .= theme('image', array('path' => file_create_url($item['icon'])));
  $output .= '<span class="messenger label">' . check_plain($item['label']) . '</span>';
  $output .= ': ';
  $output .= '<span class="messenger text">' . check_plain($item['id']) . '</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme company map item.
 */
function theme_company_info_item_maps($variables) { 
  $item = $variables['item'];
  $key = $variables['key'];
  
  $output = '<div class="item-' . $key . '">';
  $output .= '<span class="map label">' . l($item['label'], 'company-info/maps', array('fragment' => 'map-' . $key)) . '</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme popup for the google map.
 */
function theme_company_info_gmap_popup($variables) {
  $output = '<div class="popup">';    
  $output .= '<div class="location">';
  $output .= check_plain($variables['item']['location']);
  $output .= '</div>';
        
  if ($variables['item']['description']) {
    $output .= '<div class="description">';
    $output .= filter_xss($variables['item']['description']);
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}

/**
 * Theme list of google maps.
 */
function theme_company_info_maps_page($variables) {
  
  $output = '';
  $settings = variable_get('company_info_settings', array());
  $information = variable_get('company_info_info', array());
  $height = $settings['maps']['dynamic']['height'];
  $width = $settings['maps']['dynamic']['width'];
  
  if (!empty($information['maps']['items'])) {
    foreach ($information['maps']['items'] as $key => $item) {
      $label = check_plain($item['label']);
      if ($settings['maps']['full_style'] == 'static') {
        $output .= '<div id="map-' . $key . '">';
        $output .= '<div class="static">';
        $output .= '<img src="' . check_url($item['static']) . '" title="' . $label . '" alt="' . $label . '" />';
        $output .= '</div>';
        
        if ($item['description']) {
          $output .= '<div class="description">' . filter_xss($item['description']) . '</div>';
        }
        $output .= '</div>';
      }
      else {
        $output .= '<div id="map-' . $key . '" class="dynamic-map">';
        $output .= '</div>';
        
        $popup = theme('company_info_gmap_popup', array('item' => $item));
        
        drupal_add_css('.dynamic-map {width: ' . $width . 'px; height: ' . $height . 'px}', 'inline');
        company_info_add_dynamic_map($key, $item, $settings, $popup);
      }
    }
  }
  return $output;
}
