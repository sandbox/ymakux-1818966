<?php


function company_info_settings_form($form, &$form_state) {

  $form['#tree'] = TRUE;
  $settings = variable_get('company_info_settings', array());
  
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  
  $form['company'] = array(
    '#title' => t('Company info'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'vertical_tabs',
  );
  
  $form['maps'] = array(
    '#title' => t('Goole Maps'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'vertical_tabs',
  );
  
  $form['maps']['dynamic'] = array(
    '#title' => t('Dynamic map'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['maps']['static'] = array(
    '#title' => t('Static map'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['maps']['key'] = array(
    '#title' => t('Google API key'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['key']) ? $settings['maps']['key'] : '',
  );
  
  $form['maps']['static']['width'] = array(
    '#title' => t('Map width'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['static']['width']) ? $settings['maps']['static']['width'] : 500,
  );
  
  $form['maps']['static']['height'] = array(
    '#title' => t('Map height'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['static']['height']) ? $settings['maps']['static']['height'] : 500,
  );
  
  $form['maps']['static']['zoom'] = array(
    '#title' => t('Zoom'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['static']['zoom']) ? $settings['maps']['static']['zoom'] : 15,
  );
  
  $form['maps']['static']['image_style'] = array(
    '#title' => t('Map preview image style'),
    '#type' => 'select',
    '#options' => image_style_options(),
    '#default_value' => isset($settings['maps']['static']['image_style']) ? $settings['maps']['static']['image_style'] : 0,
  );
  
  $form['maps']['dynamic']['width'] = array(
    '#title' => t('Map width'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['dynamic']['width']) ? $settings['maps']['dynamic']['width'] : 500,
  );
  
  $form['maps']['dynamic']['height'] = array(
    '#title' => t('Map height'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['dynamic']['height']) ? $settings['maps']['dynamic']['height'] : 500,
  );
  
  $form['maps']['dynamic']['zoom'] = array(
    '#title' => t('Zoom'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['maps']['dynamic']['zoom']) ? $settings['maps']['dynamic']['zoom'] : 15,
  );
  
  $form['maps']['full_style'] = array(
    '#title' => t('Full page style'),
    '#type' => 'select',
    '#options' => array('static' => t('Static'), 'dynamic' => t('Dynamic')),
    '#default_value' => isset($settings['maps']['full_style']) ? $settings['maps']['full_style'] : 'dynamic',
  );
  
  $form['maps']['link'] = array(
    '#title' => t('Linked thumblain'),
    '#type' => 'checkbox',
    '#disabled' => TRUE,
    '#default_value' => isset($settings['maps']['link']) ? $settings['maps']['link'] : 1,
    '#description' => 'So far disabled',
  );
  
  $form['company']['name'] = array(
    '#title' => t('Official name'),
    '#type' => 'textfield',
    '#default_value' => !empty($settings['company']['name']) ? $settings['company']['name'] : '',
    '#description' => '',
  );
  
  $form['company']['born'] = array(
    '#title' => t('Year of born'),
    '#type' => 'textfield',
    '#default_value' => !empty($settings['company']['born']) ? $settings['company']['born'] : '',
    '#description' => '',
  );
        
  $form['company']['copyright'] = array(
    '#title' => t('Copyright'),
    '#type' => 'textfield',
    '#default_value' => !empty($settings['company']['copyright']) ? $settings['company']['copyright'] : '%symbol %years %company_name All Rights Reserved',
    '#description' => '',
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function company_info_settings_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  unset($form_state['values']['vertical_tabs']);
  variable_set('company_info_settings', $form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
}

function company_info_admin_form($form, &$form_state) {

  $form['#tree'] = TRUE;
  $columns = company_info_types_info();
  $information = variable_get('company_info_info', array());
  
  $form['#attached']['css'][] = array(
    'data' => drupal_get_path('module' , 'company_info') . '/company_info.css',
    'type' => 'file',
  );
  
  $form['overview'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="company-info-overview">',
    '#suffix' => '</div>',
  );
  
  $i = 1;
  foreach ($information as $source_id => $source) {
    $last_class = (count($information) == $i) ? 'last' : '';
    $form['overview'][$source_id] = array(
      '#type' => 'container',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array('class' => array('company-info-column', $source_id, 'pos-' . $i, $last_class)),
    );
    
    $markup = t('Empty');
    if (!empty($information[$source_id]['items'])) {
      $markup = theme('company_info_column', array('items' => $information[$source_id], 'type' => $source_id));
    }

    if (!empty($information[$source_id]['label'])) {
      $label = t('@company_info_column', array('@company_info_column' => $information[$source_id]['label']));
    }
    else {
      $label = t('@company_info_column', array('@company_info_column' => $columns[$source_id]['label']));
    }
    
    $form['overview'][$source_id]['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Column label'),
      '#default_value' => $label,
      '#title_display' => 'invisible',
    );
    
    $form['overview'][$source_id]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => !empty($source['weight']) ? $source['weight'] : 0,
      '#delta' => 2,
    );

    $form['overview'][$source_id]['items']  = array(
      '#markup' => $markup,
    );
    
    $i++;
  }
  
  if ($information) {
    $form['overview']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('company_info_admin_form_save'),
      '#prefix' => '<div id="company-info-actions">',
    );
  
    $form['overview']['clear'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clear all. Can not be undone!'),
      '#default_value' => 0,
      '#suffix' => '</div>',
    );
  }
  
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<div style="clear:both;"><h2>' . t('Add company info') . '</h2></div>',
  );
 
  foreach ($columns as $source_id => $source) {
    $form['company_info'][$source_id] = array(
      '#type' => 'fieldset', 
      '#title' => check_plain($source['label']),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'vertical_tabs',
    );
    
    $form['company_info'][$source_id]['label'] = array(
      '#type' => 'value',
      '#value' => $source['label'],
    );

    foreach ($source['form'] as $key => $value) {
      $form['company_info'][$source_id]['form'][$key] = $value;
    }
  
    $form['company_info'][$source_id]['add'] = array(
      '#name' => $source_id,
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('company_info_admin_form_add_submit'),
      '#validate' => array('company_info_admin_form_add_validate'),
      '#states' => array(
        'enabled' => array(
          ':input[name="company_info[' . $source_id . '][form][label]"]' => array('filled' => TRUE),
        ),
      ),
    );
  }
  return $form;
}


function company_info_admin_form_save($form, &$form_state) {
  form_state_values_clean($form_state);
  $information = variable_get('company_info_info', array());
  
  if ($form_state['values']['overview']['clear']) {
    variable_del('company_info_info');
    return;
  }
  
  unset($form_state['values']['overview']['clear']);
  foreach ($form_state['values']['overview'] as $key => $value) {
    $information[$key]['label'] = $value['label'];
    $information[$key]['weight'] = $value['weight'];
  }
  uasort($information, 'drupal_sort_weight');
  variable_set('company_info_info', $information);
}

function company_info_admin_form_add_validate($form, &$form_state) {

  $clicked = $form_state['triggering_element']['#parents'][1];
  $values = $form_state['values']['company_info'][$clicked]['form'];
  $information = variable_get('company_info_info', array());
  
  if (!drupal_strlen($values['label']) && $clicked != 'misc') {
    form_set_error('company_info[' . $clicked . '][form][label', t('You have to provide a label'));
    return;
  }
  
  if (!empty($values['link'])) {
    if (!valid_url($values['link'], TRUE)) {
      form_set_error('company_info[' . $clicked . '][form][link', t('You have to provide a valid URL'));
      return;
    }
  }
  
  if (!empty($values['born'])) {
    form_set_error('company_info[' . $clicked . '][form][born', t('Please provide a numeric value'));
    return;
  }
  
  if ($clicked == 'social' || $clicked == 'messenger') {
    if ($values['icon']) {
      $file = file_load($values['icon']);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      form_set_value($form['company_info'][$clicked]['form']['icon'], $file->uri, $form_state);
    }
  }
  
  if ($clicked == 'maps') {
    $geo[] = $values['latitude'];
    $geo[] = $values['longitude'];
    
    if ($image = company_info_get_static_map($path = NULL, $geo)) {
      form_set_value($form['company_info'][$clicked]['form']['static'], $image, $form_state);
    }
    else {
      form_set_error('', t('Could not recieve the static Google Map image'));
    }
  }
}


function company_info_admin_form_add_submit($form, &$form_state) {
  
  form_state_values_clean($form_state);
  variable_set('company_info_test', $form_state['values']);
  
  $clicked = $form_state['triggering_element']['#parents'][1];
  $values = $form_state['values']['company_info'][$clicked]['form'];
  $information = variable_get('company_info_info', array());
  
  $information[$clicked]['items'][] = $form_state['values']['company_info'][$clicked]['form'];
  $information[$clicked]['label'] = $form_state['values']['company_info'][$clicked]['label'];
  variable_set('company_info_info', $information);
}