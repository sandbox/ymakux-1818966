Drupal.behaviors.companyInfoGoogleMap = {
  attach: function (context, settings) {
    document.getElementById('company-info-geo-data').style.display = 'none';

    function initialize() {
      var mapOptions = {
          center: new google.maps.LatLng(50.4501, 30.5234),
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
      var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
      var input = document.getElementById('google-map');
      var autocomplete = new google.maps.places.Autocomplete(input);
      
      autocomplete.bindTo('bounds', map);

      var infowindow = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({map: map});

      google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        input.className = '';
        
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          input.className = 'notfound';
          return;
        }

        if (place.geometry.viewport) {
          map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
        var image = new google.maps.MarkerImage(
          place.icon,
          new google.maps.Size(71, 71),
          new google.maps.Point(0, 0),
          new google.maps.Point(17, 34),
          new google.maps.Size(35, 35));
          marker.setIcon(image);
          marker.setPosition(place.geometry.location);

        var address = '';
        if (place.address_components) {
          address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
          
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        document.getElementById('geo-latitude').value=lat;
        document.getElementById('geo-longitude').value=lng;

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
  }
};